# Everything related to mail (Postfix mostly)

## postfix_reverse_hostname_access.pcre

This PCRE file contains a domain names of telecom internet providers which clients are dumb as hell and runs on their's -washing machinges- PCs unverified and oudated software (Hi, -Ped- Microsoft. Hi, Goooo -lag- gle). And all their's smart home fancy equpment too.

Use this list to filter spamers out if you are not using greylisting.

```
smtpd_client_restrictions = ...
    permit_sasl_authenticated,
    check_reverse_client_hostname_access pcre:/usr/local/etc/postfix/checks/reverse_hostname_access.pcre,
    ...
    
smtpd_recipient_restrictions = ...
    reject_unknown_reverse_client_hostname,
    check_reverse_client_hostname_access pcre:/usr/local/etc/postfix/checks/reverse_hostname_access.pcre,
    reject_unknown_client_hostname,
    ...
```

And a small message to telecom providers. Why not to set the PTR in a way so it could be easily filtered out by a signle regex rule i.e static-xx-xx-xx-xx-isp-blabla.net or static-xx-xx-xx-xx-broadband-blabla.net?


